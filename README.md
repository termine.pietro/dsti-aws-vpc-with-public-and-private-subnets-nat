# DSTI AWS VPC with Public and Private Subnets NAT

Procedure to set Amazon VPC with public and private subnets 

[Using the setup arrangment described by Amazon](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Scenario2.html)

![Alt text](imgs/diagram.jpg?raw=true "VCP")

## Create a Virtual Privite Cloud Interface using the wizard (VPC S19)

![Alt text](imgs/VPC_Wizard.png?raw=true "VCP")

Choose VPC with public and private subnets:

![Alt text](imgs/VPC_Wizard1.png?raw=true "VCP")

## Create the NAT machine using the AWS default settings

![Alt text](imgs/VPC_Wizard2.png?raw=true "VCP")

## Create machine number 1 in the public subnet (Public S19)
![Alt text](imgs/public_instance.PNG?raw=true "VCP")

## Create machine number 3 in the private subnet (Private S19)
![Alt text](imgs/private_instance.PNG?raw=true "VCP")


## Set up Security Group properties for Private, Public and Default
![Alt text](imgs/private_instance.PNG?raw=true "VCP")
